import {
  DynamoDBClient,
  PutItemCommand,
} from '@aws-sdk/client-dynamodb';
import { marshall } from '@aws-sdk/util-dynamodb';
import type { APIGatewayProxyEvent } from 'aws-lambda';
import type { CoverageConstructorParameter } from './Coverage.model';
import { Coverage } from './Coverage.model';

export const putHandler = async (event: APIGatewayProxyEvent) => {
  const {
    body: rawBody,
    headers,
    pathParameters,
  } = event;

  if (!pathParameters || !pathParameters.branch || !pathParameters.project) {
    return {
      statusCode: 400,
      body: JSON.stringify('Invalid path parameters'),
    };
  }

  const contentType = headers['content-type'] || headers['Content-Type'] || null;
  const {
    branch,
    project,
  } = pathParameters;

  if (contentType !== 'application/json') {
    return {
      statusCode: 400,
      body: JSON.stringify('Invalid content type'),
    };
  }

  const body: CoverageConstructorParameter[] = rawBody && JSON.parse(rawBody);

  if (!Array.isArray(body)) {
    return {
      statusCode: 400,
      body: JSON.stringify('Invalid body'),
    };
  }

  try {
    const coverages = body.map(Coverage.new);

    const client = new DynamoDBClient({ region: 'ap-northeast-2' });

    const command = new PutItemCommand({
      TableName: 'coverages',
      Item: marshall({
        branch,
        project,
        coverages,
      }, {
        convertClassInstanceToMap: true,
        removeUndefinedValues: true,
      }),
    });

    await client.send(command);

    return {
      statusCode: 200,
      body: 'OK',
    };
  } catch (error: unknown) {
    return {
      statusCode: 400,
      body: JSON.stringify(error instanceof Error && error?.message || 'Invalid body'),
    };
  }
};
