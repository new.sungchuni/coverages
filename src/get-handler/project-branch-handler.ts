import {
  DynamoDBClient,
  GetItemCommand,
} from '@aws-sdk/client-dynamodb';
import { marshall, unmarshall } from '@aws-sdk/util-dynamodb';
import type {
  APIGatewayProxyEvent,
  APIGatewayProxyResult,
} from 'aws-lambda';
import { concatCoverageBadges } from './concat-coverage-badges';
import type { Coverage } from '../Coverage.model';

export const projectBranchHandler = async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
  const {
    headers,
    pathParameters,
    queryStringParameters,
  } = event;

  const isValidPathParameters = pathParameters && 'branch' in pathParameters && 'project' in pathParameters;
  if (!isValidPathParameters) {
    return {
      statusCode: 400,
      body: JSON.stringify('Invalid path parameters'),
    };
  }

  const {
    branch,
    project,
  } = pathParameters;
  const client = new DynamoDBClient({ region: 'ap-northeast-2' });
  const command = new GetItemCommand({
    TableName: 'coverages',
    Key: marshall({
      branch,
      project,
    }),
  });
  const { Item } = await client.send(command);
  if (!Item) {
    return {
      statusCode: 404,
      body: JSON.stringify('Not found'),
    };
  }

  const item = unmarshall(Item) as {
    branch: string,
    project: string,
    coverages: Coverage[],
  };
  const { coverages } = item;
  const acceptHeader = headers.accept || headers['Accept'] || null;
  const isSvgRequest = (
    acceptHeader?.includes('image/svg') ||
      queryStringParameters?.format === 'svg'
  );
  if (isSvgRequest) {
    return {
      statusCode: 200,
      headers: {
        'cache-control': 'max-age=300, s-maxage=300',
        'content-type': 'image/svg+xml',
      },
      body: concatCoverageBadges(coverages),
    };
  }

  return {
    statusCode: 200,
    body: JSON.stringify(coverages),
  };
};
