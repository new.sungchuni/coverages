import type { APIGatewayProxyEvent } from 'aws-lambda';
import { appendCorsHeaders } from './append-cors-headers';
import { projectBranchHandler } from './project-branch-handler';
import { schemaHandler } from './schema-handler';

export const getHandler = (event: APIGatewayProxyEvent) => {
  const { resource } = event;
  const handler = (
    resource === '/schema' && schemaHandler ||
    resource === '/{project}/{branch}' && projectBranchHandler ||
    null
  );
  if (!handler) {
    return Promise.resolve({
      body: 'Invalid resource',
      statusCode: 404,
    });
  }

  return appendCorsHeaders(
    event,
    handler,
  );
};
