import { makeBadge } from 'badge-maker';
import type { Coverage } from '../Coverage.model';

export const concatCoverageBadges = (coverages: Coverage[]) => {
  type Badge = {
    svg: string,
    height: number,
  };

  type Accumulator = {
    badges: Badge[],
    maxWidth: number,
    sumHeight: number,
  };

  const rowGap = 4;

  const {
    badges,
    maxWidth,
    sumHeight,
  } = coverages.reduce<Accumulator>(({
    badges,
    maxWidth,
    sumHeight,
  }, {
    packageName,
    summary,
  }, index) => {
    const summaryTotalStatementsPct = summary.total?.statements?.pct ?? null;

    const svg = makeBadge({
      label: packageName || 'coverage',
      message: (
        summaryTotalStatementsPct !== null
          ? `${summaryTotalStatementsPct}%`
          : 'unknown'
      ),
      color: `hsl(${Math.trunc(Number(summaryTotalStatementsPct) * 1.2)}, 100%, 60%)`,
    })
      .replace(/<svg /, `<svg id="svg${index}" `)
      .replace(/(?<=id="|url\(#)(r|s)(?="|\))/g, `$1${index}`);

    const matches = /width="(?<width>\d+)" height="(?<height>\d+)"/.exec(svg);
    const { height, width } = matches?.groups || {};

    badges.push({
      svg,
      height: +height,
    });

    return {
      badges,
      maxWidth: Math.max(maxWidth, +width || 0),
      sumHeight: sumHeight + +height + rowGap,
    };
  }, {
    badges: [],
    maxWidth: 0,
    sumHeight: -rowGap,
  });

  const body = [
    `<svg xmlns="http://www.w3.org/2000/svg" width="${maxWidth}" height="${Math.max(sumHeight, 0)}">`,
    badges.map(
      ({ svg, height }, index) => `
        <defs>${svg}</defs>
        <use href="#svg${index}" x="0" y="${index * (height + rowGap)}" />
      `.trim(),
    ),
    '</svg>',
  ]
    .flat()
    .join('');

  return body;
};
