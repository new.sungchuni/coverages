import type {
  APIGatewayProxyEvent,
  APIGatewayProxyResult,
} from 'aws-lambda';

export const appendCorsHeaders = async (
  event: APIGatewayProxyEvent,
  handler: (event: APIGatewayProxyEvent) => Promise<APIGatewayProxyResult>,
): Promise<APIGatewayProxyResult> => {
  const response = await handler(event);

  const { headers: eventHeaders } = event;
  const originHeader = (
    eventHeaders?.origin ||
    eventHeaders?.Origin ||
    null
  );

  if (originHeader) {
    const { headers: responseHeaders = {} } = response;
    const xForwardedProtoHeader = (
      eventHeaders?.['x-forwarded-proto'] ||
      eventHeaders?.['X-Forwarded-Proto'] ||
      null
    );

    xForwardedProtoHeader && Object.assign(responseHeaders, {
      'access-control-allow-credentials': true,
    });

    Object.assign(responseHeaders, {
      'access-control-allow-origin': originHeader,
    });

    !('headers' in response) && Object.assign(response, {
      headers: responseHeaders,
    });
  }

  return response;
};
