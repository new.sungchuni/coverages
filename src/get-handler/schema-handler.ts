import {
  APIGatewayClient,
  GetExportCommand,
} from '@aws-sdk/client-api-gateway';
import { toUtf8 } from '@aws-sdk/util-utf8-node';
import type {
  APIGatewayProxyEvent,
  APIGatewayProxyResult,
} from 'aws-lambda';

export const schemaHandler = async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
  const {
    queryStringParameters,
    requestContext: {
      apiId,
      stage,
    },
  } = event;

  const client = new APIGatewayClient({ region: 'ap-northeast-2' });
  const command = new GetExportCommand({
    exportType: queryStringParameters?.export_type || 'oas30',
    restApiId: apiId,
    stageName: stage,
  });
  const response = await client.send(command);

  const { body } = response;
  if (!body) {
    return {
      statusCode: 404,
      body: JSON.stringify('Not found'),
    };
  }

  return {
    statusCode: 200,
    body: toUtf8(body),
  };
};
