import type { APIGatewayProxyHandler } from 'aws-lambda';
import { getHandler } from './get-handler';
import { putHandler } from './put-handler';

export const handler: APIGatewayProxyHandler = (event) => {
  const { httpMethod } = event;

  return Promise.resolve(
    (httpMethod === 'HEAD' || httpMethod === 'GET') && getHandler(event) ||
    (httpMethod === 'POST' || httpMethod === 'PUT') && putHandler(event) ||
    {
      statusCode: 400,
      body: JSON.stringify('Invalid http method'),
    },
  );
};
