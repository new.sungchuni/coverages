export class CoverageData {
  #total: number | null = null;
  #covered: number | null = null;
  #skipped: number | null = null;
  #pct: number | null = null;

  static new() {
    return new CoverageData();
  }

  get total() {
    return this.#total;
  }

  get covered() {
    return this.#covered;
  }

  get skipped() {
    return this.#skipped;
  }

  get pct() {
    return this.#pct;
  }

  setTotal(total: number | null) {
    this.#total = total;
    return this;
  }

  setCovered(covered: number | null) {
    this.#covered = covered;
    return this;
  }

  setSkipped(skipped: number | null) {
    this.#skipped = skipped;
    return this;
  }

  setPct(pct: number | null) {
    this.#pct = pct;
    return this;
  }
}

export class CoverageSummary {
  #fileName: 'total' | string;
  #statements: CoverageData | null = null;
  #branches: CoverageData | null = null;
  #functions: CoverageData | null = null;
  #lines: CoverageData | null = null;

  constructor(fileName: string) {
    this.#fileName = fileName;
  }

  static new(fileName: string) {
    return new CoverageSummary(fileName);
  }

  get fileName() {
    return this.#fileName;
  }

  get statements() {
    return this.#statements;
  }

  get branches() {
    return this.#branches;
  }

  get functions() {
    return this.#functions;
  }

  get lines() {
    return this.#lines;
  }

  setStatements(coverageData: CoverageData) {
    this.#statements = coverageData;
    return this;
  }

  setBranches(coverageData: CoverageData) {
    this.#branches = coverageData;
    return this;
  }

  setFunctions(coverageData: CoverageData) {
    this.#functions = coverageData;
    return this;
  }

  setLines(coverageData: CoverageData) {
    this.#lines = coverageData;
    return this;
  }
}

export type CoverageConstructorParameter = {
  packageName: string,
  summary: Record<string, CoverageSummary>,
};

export class Coverage {
  packageName: string;
  summary: Record<string, CoverageSummary>;

  constructor({
    packageName,
    summary,
  }: CoverageConstructorParameter) {
    this.packageName = packageName;
    this.summary = summary;
  }

  static new(parameter: CoverageConstructorParameter) {
    return new Coverage(parameter);
  }
}
