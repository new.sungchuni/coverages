import commonjs from '@rollup/plugin-commonjs';
import json from '@rollup/plugin-json';
import { nodeResolve } from '@rollup/plugin-node-resolve';
import clean from '@rollup-extras/plugin-clean';
import { defineConfig } from 'rollup';
import swc from 'rollup-plugin-swc';

export default defineConfig({
  input: 'src/index.ts',
  output: {
    chunkFileNames: '[name].[hash].mjs',
    dir: 'build',
    entryFileNames: 'index.mjs',
    format: 'es',
    manualChunks(id) {
      if (id.includes('node_modules')) {
        return 'lib';
      }
    },
  },
  external: (id) => (
    id.startsWith('@aws-sdk') ||
    id === 'fs' ||
    id === 'util'
  ),
  plugins: [
    clean('build'),
    commonjs(),
    json(),
    nodeResolve({
      extensions: ['.js', '.ts'],
    }),
    swc.default(),
  ],
});
