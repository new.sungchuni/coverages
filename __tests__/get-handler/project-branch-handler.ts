import { DynamoDBClient } from '@aws-sdk/client-dynamodb';
import { marshall } from '@aws-sdk/util-dynamodb';
import { APIGatewayProxyEventFactory } from '../../__factories__/APIGatewayProxyEvent.factory';
import { projectBranchHandler } from '../../src/get-handler/project-branch-handler';

jest.mock('../../src/get-handler/concat-coverage-badges', () => ({
  concatCoverageBadges: jest.fn(() => 'test-badge'),
}));

describe('projectBranchHandler', () => {
  const client = new DynamoDBClient({ region: 'ap-northeast-2' });
  const mockedClientSend = jest.mocked(client.send);
  mockedClientSend.mockImplementation(() => ({
    Item: marshall({
      project: 'test-project',
      branch: 'test-branch',
      coverages: [
        {
          packageName: 'test-package',
          summary: {
            total: {
              statements: {
                pct: 75,
              },
            },
          },
        },
      ],
    }),
  }));

  it('should return 200 when project and branch exists', async () => {
    const event = APIGatewayProxyEventFactory.build({
      pathParameters: {
        project: 'test-project',
        branch: 'test-branch',
      },
    });
    const response = await projectBranchHandler(event);

    expect(response).toHaveProperty('statusCode', 200);
    expect(response).toHaveProperty('body');

    const coverages = JSON.parse(response.body);
    expect(coverages).toHaveLength(1);

    const [first] = coverages;
    expect(first).toHaveProperty('packageName', 'test-package');
    expect(first).toHaveProperty('summary', {
      total: {
        statements: {
          pct: 75,
        },
      },
    });
  });

  it('should return 400 when missing project pathParameters', async () => {
    const event = APIGatewayProxyEventFactory.build({
      pathParameters: {
        branch: 'test-branch',
      },
    });

    const response = await projectBranchHandler(event);

    expect(response).toHaveProperty('statusCode', 400);
  });

  it('should return 400 when missing branch pathParameters', async () => {
    const event = APIGatewayProxyEventFactory.build({
      pathParameters: {
        project: 'test-project',
      },
    });

    const response = await projectBranchHandler(event);

    expect(response).toHaveProperty('statusCode', 400);
  });

  it('should return 404 when GetItemCommand response null Item', async () => {
    mockedClientSend.mockImplementationOnce(() => ({
      Item: null,
    }));

    const event = APIGatewayProxyEventFactory.build({
      pathParameters: {
        project: 'test-project',
        branch: 'test-branch',
      },
    });

    const response = await projectBranchHandler(event);

    expect(response).toHaveProperty('statusCode', 404);
  });

  it('should return image/svg+xml when Accept header is image/svg+xml', async () => {
    const event = APIGatewayProxyEventFactory.build({
      pathParameters: {
        project: 'test-project',
        branch: 'test-branch',
      },
      headers: {
        Accept: 'image/svg+xml',
      },
    });

    const response = await projectBranchHandler(event);

    expect(response).toHaveProperty('statusCode', 200);
    expect(response).toHaveProperty('headers');
    expect(response.headers).toHaveProperty('content-type', 'image/svg+xml');
    expect(response.headers).toHaveProperty('cache-control', 'max-age=300, s-maxage=300');
  });

  it('should return image/svg+xml when accept header is image/svg+xml', async () => {
    const event = APIGatewayProxyEventFactory.build({
      pathParameters: {
        project: 'test-project',
        branch: 'test-branch',
      },
      headers: {
        accept: 'image/svg+xml',
      },
    });

    const response = await projectBranchHandler(event);

    expect(response).toHaveProperty('statusCode', 200);
    expect(response).toHaveProperty('headers');
    expect(response.headers).toHaveProperty('content-type', 'image/svg+xml');
    expect(response.headers).toHaveProperty('cache-control', 'max-age=300, s-maxage=300');
  });

  it('should return image/svg+xml when format query parameter is svg', async () => {
    const event = APIGatewayProxyEventFactory.build({
      pathParameters: {
        project: 'test-project',
        branch: 'test-branch',
      },
      queryStringParameters: {
        format: 'svg',
      },
    });

    const response = await projectBranchHandler(event);

    expect(response).toHaveProperty('statusCode', 200);
    expect(response).toHaveProperty('headers');
    expect(response.headers).toHaveProperty('content-type', 'image/svg+xml');
    expect(response.headers).toHaveProperty('cache-control', 'max-age=300, s-maxage=300');
  });
});
