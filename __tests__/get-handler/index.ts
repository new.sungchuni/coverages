import { APIGatewayProxyEventFactory } from '../../__factories__/APIGatewayProxyEvent.factory';
import { getHandler } from '../../src/get-handler';

jest.mock('../../src/get-handler/schema-handler', () => ({
  schemaHandler: jest.fn(() => ({
    statusCode: 200,
  })),
}));

jest.mock('../../src/get-handler/project-branch-handler', () => ({
  projectBranchHandler: jest.fn(() => ({
    statusCode: 200,
  })),
}));

jest.mock('../../src/get-handler/append-cors-headers', () => ({
  appendCorsHeaders: jest.fn((event, handler) => handler(event)),
}));

describe('getHandler', () => {
  it('should return 200 when /schema path', async () => {
    const event = APIGatewayProxyEventFactory.build({
      resource: '/schema',
      pathParameters: null,
    });

    const response = await getHandler(event);

    expect(response).toHaveProperty('statusCode', 200);
  });

  it('should return 200 when /{project}/{branch} resource', async () => {
    const event = APIGatewayProxyEventFactory.build({
      resource: '/{project}/{branch}',
      pathParameters: {
        project: 'test-project',
        branch: 'test-branch',
      },
    });

    const response = await getHandler(event);

    expect(response).toHaveProperty('statusCode', 200);
  });

  it('should return 404 when invalid resourece', async () => {
    const event = APIGatewayProxyEventFactory.build({
      resource: '/invalid-resource',
    });

    const response = await getHandler(event);

    expect(response).toHaveProperty('statusCode', 404);
  });
});
