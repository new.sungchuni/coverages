import { APIGatewayProxyEventFactory } from '../../__factories__/APIGatewayProxyEvent.factory';
import { appendCorsHeaders } from '../../src/get-handler/append-cors-headers';

describe('appendCorsHeaders', () => {
  const handler = () => Promise.resolve({
    statusCode: 200,
    body: JSON.stringify({}),
  });

  it('should return 200 with access-control-allow-origin headers', async () => {
    const origin = 'http://localhost:3000';
    const response = await appendCorsHeaders(
      APIGatewayProxyEventFactory.build({
        headers: {
          origin,
        },
      }),
      handler,
    );

    expect(response).toHaveProperty('statusCode', 200);
    expect(response).toHaveProperty('headers');
    expect(response.headers).toHaveProperty('access-control-allow-origin', origin);
    expect(response.headers).not.toHaveProperty('access-control-allow-credentials');
  });

  it('should return 200 with access-control-allow-credentials header when receive x-forwarded-proto header', async () => {
    const origin = 'http://localhost:3000';
    const response = await appendCorsHeaders(
      APIGatewayProxyEventFactory.build({
        headers: {
          origin,
          'x-forwarded-proto': 'https',
        },
      }),
      handler,
    );

    expect(response).toHaveProperty('statusCode', 200);
    expect(response).toHaveProperty('headers');
    expect(response.headers).toHaveProperty('access-control-allow-origin', origin);
    expect(response.headers).toHaveProperty('access-control-allow-credentials', true);
  });

  it('should return 200 with access-control-allow-credentials header when receive camelized X-Forwarded-Proto header', async () => {
    const origin = 'http://localhost:3000';
    const response = await appendCorsHeaders(
      APIGatewayProxyEventFactory.build({
        headers: {
          origin,
          'X-Forwarded-Proto': 'https',
        },
      }),
      handler,
    );

    expect(response).toHaveProperty('statusCode', 200);
    expect(response).toHaveProperty('headers');
    expect(response.headers).toHaveProperty('access-control-allow-origin', origin);
    expect(response.headers).toHaveProperty('access-control-allow-credentials', true);
  });

  it('should return 200 with access-control-allow-origin header when receive capitialized Origin header', async () => {
    const origin = 'http://localhost:3000';
    const response = await appendCorsHeaders(
      APIGatewayProxyEventFactory.build({
        headers: {
          Origin: origin,
        },
      }),
      handler,
    );

    expect(response).toHaveProperty('statusCode', 200);
    expect(response).toHaveProperty('headers');
    expect(response.headers).toHaveProperty('access-control-allow-origin', origin);
    expect(response.headers).not.toHaveProperty('access-control-allow-credentials');
  });

  it('should return 200 without access-control-allow-origin header when not receive origin header', async () => {
    const response = await appendCorsHeaders(
      APIGatewayProxyEventFactory.build({
        headers: {},
      }),
      handler,
    );

    expect(response).toHaveProperty('statusCode', 200);
    expect(response).not.toHaveProperty('headers');
  });
});
