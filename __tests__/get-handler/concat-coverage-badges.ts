import {
  CoverageData,
  CoverageSummary,
} from '../../src/Coverage.model';
import { concatCoverageBadges } from '../../src/get-handler/concat-coverage-badges';

describe('concatCoverageBadges', () => {
  it('should return empty svg element when no coverages', () => {
    const coverages = [];
    const result = concatCoverageBadges(coverages);

    expect(result).toBe('<svg xmlns="http://www.w3.org/2000/svg" width="0" height="0"></svg>');
  });

  it('should return badge when one coverage', () => {
    const coverages = [
      {
        packageName: 'test-package',
        summary: {
          total: CoverageSummary.new('total').setStatements(
            CoverageData.new().setPct(75),
          ),
        },
      },
    ];
    const result = concatCoverageBadges(coverages);

    expect(result).toMatchSnapshot();
  });

  it('should return badges when multiple coverages', () => {
    const coverages = [
      {
        packageName: 'test-package-1',
        summary: {
          total: CoverageSummary.new('total').setStatements(
            CoverageData.new().setPct(75),
          ),
        },
      },
      {
        packageName: 'test-package-2',
        summary: {
          total: CoverageSummary.new('total').setStatements(
            CoverageData.new().setPct(50),
          ),
        },
      },
    ];
    const result = concatCoverageBadges(coverages);

    expect(result).toMatchSnapshot();
  });

  it('should return unknown badge with null coverage', () => {
    const coverages = [
      {
        packageName: 'test-package-2',
        summary: {
          total: CoverageSummary.new('total').setStatements(
            CoverageData.new().setPct(null),
          ),
        },
      },
    ];
    const result = concatCoverageBadges(coverages);

    expect(result).toMatchSnapshot();
  });
});
