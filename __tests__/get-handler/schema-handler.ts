import { APIGatewayClient, GetExportCommand } from '@aws-sdk/client-api-gateway';
import { APIGatewayProxyEventFactory } from '../../__factories__/APIGatewayProxyEvent.factory';
import { schemaHandler } from '../../src/get-handler/schema-handler';

describe('schemaHandler', () => {
  const client = new APIGatewayClient({ region: 'ap-northeast-2' });
  const mockedClientSend = jest.mocked(client.send);
  mockedClientSend.mockImplementation(() => ({
    body: new Uint8Array(),
  }));

  const mockedGetExportCommand = jest.mocked(GetExportCommand);

  const apiId = 'test-api-id';
  const stage = 'test-stage';

  it('should instance GetExportCommand with default "oas30" exportType', async () => {
    const event = APIGatewayProxyEventFactory.build({
      queryStringParameters: {},
      requestContext: {
        apiId,
        stage,
      },
    });

    await schemaHandler(event);

    expect(mockedGetExportCommand).toHaveBeenCalledWith({
      exportType: 'oas30',
      restApiId: apiId,
      stageName: stage,
    });
  });

  it('should instance GetExportCommand with "oas30" exportType', async () => {
    const event = APIGatewayProxyEventFactory.build({
      queryStringParameters: {
        export_type: 'oas30',
      },
      requestContext: {
        apiId,
        stage,
      },
    });

    await schemaHandler(event);

    expect(mockedGetExportCommand).toHaveBeenCalledWith({
      exportType: 'oas30',
      restApiId: apiId,
      stageName: stage,
    });
  });

  it('should instance GetExportCommand with "swagger" exportType', async () => {
    const event = APIGatewayProxyEventFactory.build({
      queryStringParameters: {
        export_type: 'swagger',
      },
      requestContext: {
        apiId,
        stage,
      },
    });

    await schemaHandler(event);

    expect(mockedGetExportCommand).toHaveBeenCalledWith({
      exportType: 'swagger',
      restApiId: apiId,
      stageName: stage,
    });
  });

  it('should return 404 when APIGatewayClient response null body', async () => {
    const event = APIGatewayProxyEventFactory.build({
      queryStringParameters: {},
      requestContext: {
        apiId,
        stage,
      },
    });

    mockedClientSend.mockImplementationOnce(() => ({
      body: null,
    }));

    const response = await schemaHandler(event);

    expect(response.statusCode).toBe(404);
  });
});
