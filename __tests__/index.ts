import type { Context } from 'aws-lambda';
import produce from 'immer';
import { APIGatewayProxyEventFactory } from '../__factories__/APIGatewayProxyEvent.factory';
import { handler } from '../src';

jest.mock('../src/get-handler', () => ({
  getHandler: jest.fn(() => ({
    statusCode: 200,
  })),
}));

jest.mock('../src/put-handler', () => ({
  putHandler: jest.fn(() => ({
    statusCode: 200,
  })),
}));

describe('handler', () => {
  const apiGatewayProxyEventFactoryParamsBase = {
    body: JSON.stringify([
      {
        packageName: 'test-package',
        total: {
          statements: {
            pct: 100,
          },
        },
      },
    ]),
    headers: {
      'content-type': 'application/json',
    },
    httpMethod: 'HEAD',
    pathParameters: {
      branch: 'test-branch',
      project: 'test-project',
    },
  };

  it('should return 200 when HEAD method', async () => {
    const event = APIGatewayProxyEventFactory.build(
      produce(apiGatewayProxyEventFactoryParamsBase, (draft) => {
        draft.httpMethod = 'HEAD';
      }),
    );

    const response = await handler(event, {} as Context, () => {});

    expect(response).toHaveProperty('statusCode', 200);
  });

  it('should return 200 when GET method', async () => {
    const event = APIGatewayProxyEventFactory.build(
      produce(apiGatewayProxyEventFactoryParamsBase, (draft) => {
        draft.httpMethod = 'GET';
      }),
    );

    const response = await handler(event, {} as Context, () => {});

    expect(response).toHaveProperty('statusCode', 200);
  });

  it('should return 200 when POST method', async () => {
    const event = APIGatewayProxyEventFactory.build(
      produce(apiGatewayProxyEventFactoryParamsBase, (draft) => {
        draft.httpMethod = 'POST';
      }),
    );

    const response = await handler(event, {} as Context, () => {});

    expect(response).toHaveProperty('statusCode', 200);
  });

  it('should return 200 when PUT method', async () => {
    const event = APIGatewayProxyEventFactory.build(
      produce(apiGatewayProxyEventFactoryParamsBase, (draft) => {
        draft.httpMethod = 'PUT';
      }),
    );

    const response = await handler(event, {} as Context, () => {});

    expect(response).toHaveProperty('statusCode', 200);
  });

  it('should return 400 when invalid methods', async () => {
    const event = APIGatewayProxyEventFactory.build(
      produce(apiGatewayProxyEventFactoryParamsBase, (draft) => {
        draft.httpMethod = 'DELETE';
      }),
    );

    const response = await handler(event, {} as Context, () => {});

    expect(response).toHaveProperty('statusCode', 400);
  });
});
