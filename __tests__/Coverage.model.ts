import type { CoverageConstructorParameter } from '../src/Coverage.model';
import {
  CoverageData,
  CoverageSummary,
  Coverage,
} from '../src/Coverage.model';

describe('CoverageData', () => {
  it('should be created', () => {
    const coverageData = new CoverageData;
    expect(coverageData).toBeInstanceOf(CoverageData);
    expect(coverageData).toHaveProperty('total', null);
    expect(coverageData).toHaveProperty('covered', null);
    expect(coverageData).toHaveProperty('skipped', null);
    expect(coverageData).toHaveProperty('pct', null);

    coverageData.setPct(100);
    expect(coverageData).toHaveProperty('pct', 100);

    coverageData.setTotal(10);
    expect(coverageData).toHaveProperty('total', 10);

    coverageData.setCovered(10);
    expect(coverageData).toHaveProperty('covered', 10);

    coverageData.setSkipped(0);
    expect(coverageData).toHaveProperty('skipped', 0);
  });

  it('should be created by static method', () => {
    const coverageData = CoverageData.new().setPct(100);
    expect(coverageData).toBeInstanceOf(CoverageData);
    expect(coverageData).toHaveProperty('pct', 100);
  });
});

describe('CoverageSummary', () => {
  it('should be created', () => {
    const coverageSummary = new CoverageSummary('total');
    expect(coverageSummary).toBeInstanceOf(CoverageSummary);
    expect(coverageSummary).toHaveProperty('fileName', 'total');
    expect(coverageSummary).toHaveProperty('lines', null);
    expect(coverageSummary).toHaveProperty('functions', null);
    expect(coverageSummary).toHaveProperty('statements', null);
    expect(coverageSummary).toHaveProperty('branches', null);

    const linesCoverageData = CoverageData.new();
    coverageSummary.setLines(linesCoverageData);
    expect(coverageSummary).toHaveProperty('lines');
    expect(coverageSummary.lines).toBe(linesCoverageData);

    const functionsCoverageData = CoverageData.new();
    coverageSummary.setFunctions(functionsCoverageData);
    expect(coverageSummary).toHaveProperty('functions');

    const statementsCoverageData = CoverageData.new();
    coverageSummary.setStatements(statementsCoverageData);
    expect(coverageSummary).toHaveProperty('statements');

    const branchesCoverageData = CoverageData.new();
    coverageSummary.setBranches(branchesCoverageData);
    expect(coverageSummary).toHaveProperty('branches');
  });

  it('should be created by static method', () => {
    const coverageSummary = CoverageSummary.new('total');
    expect(coverageSummary).toBeInstanceOf(CoverageSummary);
    expect(coverageSummary).toHaveProperty('fileName', 'total');
    expect(coverageSummary).toHaveProperty('lines', null);
    expect(coverageSummary).toHaveProperty('functions', null);
    expect(coverageSummary).toHaveProperty('statements', null);
    expect(coverageSummary).toHaveProperty('branches', null);
  });
});

describe('Coverage', () => {
  it('should be created', () => {
    const CoverageConstructorParameter: CoverageConstructorParameter = {
      packageName: 'test-package',
      summary: {
        total: CoverageSummary.new('total').setStatements(
          CoverageData.new().setPct(100),
        ),
      },
    };
    const coverage = new Coverage(CoverageConstructorParameter);
    expect(coverage).toBeInstanceOf(Coverage);
    expect(coverage).toHaveProperty('packageName', 'test-package');
    expect(coverage).toHaveProperty('summary');
    expect(coverage.summary).toMatchObject({
      total: {
        fileName: 'total',
        statements: {
          pct: 100,
        },
      },
    });
  });

  it('should be created by static method', () => {
    const CoverageConstructorParameter: CoverageConstructorParameter = {
      packageName: 'test-package',
      summary: {
        total: CoverageSummary.new('total').setStatements(
          CoverageData.new().setPct(100),
        ),
      },
    };
    const coverage = Coverage.new(CoverageConstructorParameter);
    expect(coverage).toBeInstanceOf(Coverage);
    expect(coverage).toHaveProperty('packageName', 'test-package');
    expect(coverage).toHaveProperty('summary');
    expect(coverage.summary).toMatchObject({
      total: {
        fileName: 'total',
        statements: {
          pct: 100,
        },
      },
    });
  });
});
