import { DynamoDBClient } from '@aws-sdk/client-dynamodb';
import produce from 'immer';
import { APIGatewayProxyEventFactory } from '../__factories__/APIGatewayProxyEvent.factory';
import { putHandler } from '../src/put-handler';

describe('putHandler', () => {
  const apiGatewayProxyEventFactoryParamsBase = {
    body: JSON.stringify([
      {
        packageName: 'test-package',
        summary: {
          total: {
            statements: {
              pct: 100,
            },
          },
        },
      },
    ]),
    headers: {
      'content-type': 'application/json',
    },
    pathParameters: {
      branch: 'test-branch',
      project: 'test-project',
    },
  };

  it('should return 200', async () => {
    const event = APIGatewayProxyEventFactory.build(
      apiGatewayProxyEventFactoryParamsBase,
    );

    const response = await putHandler(event);

    expect(response).toHaveProperty('statusCode', 200);
  });

  it('should return 400 when invalid content-type', async () => {
    const event = APIGatewayProxyEventFactory.build(
      produce(apiGatewayProxyEventFactoryParamsBase, (draft) => {
        draft.headers['content-type'] = 'invalid-content-type';
      }),
    );

    const response = await putHandler(event);

    expect(response).toHaveProperty('statusCode', 400);
  });

  it('should return 400 when invalid body not array', async () => {
    const event = APIGatewayProxyEventFactory.build(
      produce(apiGatewayProxyEventFactoryParamsBase, (draft) => {
        draft.body = JSON.stringify({});
      }),
    );

    const response = await putHandler(event);

    expect(response).toHaveProperty('statusCode', 400);
  });

  it('should return 400 when invalid branch path parameter', async () => {
    const event = APIGatewayProxyEventFactory.build(
      produce(apiGatewayProxyEventFactoryParamsBase, (draft) => {
        draft.pathParameters.branch = '';
      }),
    );

    const response = await putHandler(event);

    expect(response).toHaveProperty('statusCode', 400);
  });

  it('should return 400 when invalid project path parameter', async () => {
    const event = APIGatewayProxyEventFactory.build(
      produce(apiGatewayProxyEventFactoryParamsBase, (draft) => {
        draft.pathParameters.project = '';
      }),
    );

    const response = await putHandler(event);

    expect(response).toHaveProperty('statusCode', 400);
  });

  it('should return 400 when PutItemCommand throw specific error message', async () => {
    const errorMessage = 'test error';
    const client = new DynamoDBClient({ region: 'ap-northeast-2' });
    jest.mocked(client.send).mockImplementationOnce(() => {
      throw new Error(errorMessage);
    });

    const event = APIGatewayProxyEventFactory.build(
      apiGatewayProxyEventFactoryParamsBase,
    );

    const response = await putHandler(event);

    expect(response).toHaveProperty('statusCode', 400);
    expect(response).toHaveProperty('body', JSON.stringify(errorMessage));
  });

  it('should return 400 when PutItemCommand throw unknown error', async () => {
    const client = new DynamoDBClient({ region: 'ap-northeast-2' });
    jest.mocked(client.send).mockImplementationOnce(() => {
      throw null;
    });

    const event = APIGatewayProxyEventFactory.build(
      apiGatewayProxyEventFactoryParamsBase,
    );

    const response = await putHandler(event);

    expect(response).toHaveProperty('statusCode', 400);
    expect(response).toHaveProperty('body', JSON.stringify('Invalid body'));
  });
});
