import type { APIGatewayProxyEvent } from 'aws-lambda';
import { Factory } from 'fishery';
import produce from 'immer';

export const APIGatewayProxyEventFactory = Factory.define<APIGatewayProxyEvent>(({ params }) => produce({
  resource: '/schema',
  path: '/schema',
  httpMethod: 'GET',
  headers: {
    accept: '*/*',
    'accept-encoding': 'gzip, deflate, br',
    'accept-language': 'ko-KR,ko;q=0.9,en-US;q=0.8,en;q=0.7',
    Host: 'coverages.sungchuni.site',
    origin: 'https://editor.swagger.io',
    referer: 'https://editor.swagger.io/',
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36',
    'X-Amzn-Trace-Id': 'Root=1-63d9f67e-467d11396a63f2c42745c192',
    'X-Forwarded-For': '0.0.0.0',
    'X-Forwarded-Port': '443',
    'X-Forwarded-Proto': 'https',
  },
  multiValueHeaders: {
    accept: ['*/*'],
    'accept-encoding': ['gzip, deflate, br'],
    'accept-language': ['ko-KR,ko;q=0.9,en-US;q=0.8,en;q=0.7'],
    Host: ['coverages.sungchuni.site'],
    origin: ['https://editor.swagger.io'],
    referer: ['https://editor.swagger.io/'],
    'User-Agent': ['Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36'],
    'X-Amzn-Trace-Id': ['Root=1-63d9f67e-467d11396a63f2c42745c192'],
    'X-Forwarded-For': ['0.0.0.0'],
    'X-Forwarded-Port': ['443'],
    'X-Forwarded-Proto': ['https'],
  },
  queryStringParameters: { export_type: 'swagger' },
  multiValueQueryStringParameters: { export_type: ['swagger'] },
  pathParameters: null,
  stageVariables: null,
  requestContext: {
    resourceId: 'dqrjh7',
    resourcePath: '/schema',
    httpMethod: 'GET',
    extendedRequestId: 'fpNz3FbFoE0FbrQ=',
    requestTime: '01/Feb/2023:05:19:58 +0000',
    path: '/v1/schema',
    accountId: '160653022635',
    protocol: 'HTTP/1.1',
    stage: 'v1',
    domainPrefix: 'coverages',
    requestTimeEpoch: 1675228798941,
    requestId: '584c3055-f43f-465f-a3cf-151bc9f898c1',
    identity: {
      apiKey: null,
      apiKeyId: null,
      clientCert: null,
      cognitoIdentityPoolId: null,
      accountId: null,
      cognitoIdentityId: null,
      caller: null,
      sourceIp: '0.0.0.0',
      principalOrgId: null,
      accessKey: null,
      cognitoAuthenticationType: null,
      cognitoAuthenticationProvider: null,
      userArn: null,
      userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36',
      user: null,
    },
    domainName: 'coverages.sungchuni.site',
    apiId: 'zfyicz5xi4',
    authorizer: null,
  },
  body: null,
  isBase64Encoded: false,
}, (draft) => Object.assign(draft, params)));
