---
marp: true
paginate: true
header: 서버리스로 만드는 테스트 커버리지 배지 서비스
---

<!-- _header: '' -->

# 서버리스로 만드는 테스트 커버리지 배지 서비스

---

<!-- _footer: 2min -->

박성천
new.sungchuni@gmail.com

---

<!-- _footer: 1min -->

## 주제

테스트 커버리지
API 게이트웨이
"coverages"

---

### 테스트 커버리지

---

<!-- _footer: 30sec -->

![w:1024](https://istanbul.js.org/assets/terminal.png)

---

<!-- _footer: 30sec -->

![w:1024](https://istanbul.js.org/assets/browser.png)

---

<!-- _footer: 1min -->

statements
branches
functions
lines

---

<!-- _footer: 5min -->

## 커버리지 지표 실습
```ts
const clamp = (value: number, min: number, max: number) => {
  if (value < min) {
    return min;
  }

  if (value > max) {
    return max;
  }

  return value;
};
```

---

<!-- _footer: 1min -->

![w:800](https://martinfowler.com/bliki/images/testCoverage/sketch.png)

_(https://martinfowler.com/bliki/TestCoverage.html)_

---

### API 게이트웨이

---

<!-- _footer: 30sec -->

> Amazon API Gateway
모든 규모의 API를 생성, 유지 관리 및 보호

---

<!-- _footer: 30sec -->

![w:1024](https://d1.awsstatic.com/serverless/New-API-GW-Diagram.c9fc9835d2a9aa00ef90d0ddc4c6402a2536de0d.png)

---

<!-- _footer: 2min -->

## 이득

스키마 작성, 관리, 전파
리졸버 캡슐화
보안, 인증, 권한
캐싱...?

---

<!-- _footer: 1min -->

## Amazon API Gateway 상품 목록

HTTP API
REST API
WebSocket API

---

### "coverages"

---

<!-- _footer: 1min -->

## AWS resources

API Gateway
Lambda
DynamoDB

---

<!-- _footer: 3min -->

## 같이 보기

DynamoDB 테이블

---

<!-- _footer: 3min -->

## 함께 보기

API Gateway 리소스, 메서드, 모델

---

<!-- _footer: 5min -->

## 살펴 보기

Lambda 함수 PUT 처리기

---

<!-- _footer: 5min -->

## 더 보기

Lambda 함수 GET 처리기

---

<!-- _footer: 5min -->

## 서비스 직접 사용해 보기

---

<!-- _footer: 10min -->

## 코드 리뷰의 시간

---

(end of document)
