resource "aws_dynamodb_table" "main" {
  billing_mode   = "PROVISIONED"
  hash_key       = "project"
  name           = "coverages"
  range_key      = "branch"
  read_capacity  = 10
  stream_enabled = false
  table_class    = "STANDARD"
  write_capacity = 10

  attribute {
    name = "branch"
    type = "S"
  }

  attribute {
    name = "project"
    type = "S"
  }

  point_in_time_recovery {
    enabled = false
  }

  timeouts {}
}
