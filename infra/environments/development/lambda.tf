data "archive_file" "main" {
  type        = "zip"
  source_dir  = "${path.module}/../../../build"
  output_path = "${path.module}/../../../dist/main.zip"
}

resource "aws_lambda_function" "main" {
  architectures = [
    "x86_64",
  ]
  filename                       = data.archive_file.main.output_path
  function_name                  = "coverages"
  handler                        = "index.handler"
  layers                         = []
  memory_size                    = 128
  package_type                   = "Zip"
  publish                        = false
  reserved_concurrent_executions = -1
  role                           = aws_iam_role.lambda.arn
  runtime                        = "nodejs18.x"
  source_code_hash               = data.archive_file.main.output_base64sha256
  timeout                        = 10

  ephemeral_storage {
    size = 512
  }

  timeouts {}

  tracing_config {
    mode = "PassThrough"
  }
}

resource "aws_lambda_permission" "apigateway_lambda" {
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.main.function_name
  principal     = "apigateway.amazonaws.com"
  source_arn    = "${aws_api_gateway_rest_api.main.execution_arn}/*"
}
