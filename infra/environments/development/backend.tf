terraform {
  required_version = ">= 1.3.9"

  required_providers {
    archive = {
      source  = "hashicorp/archive"
      version = "~> 2.3.0"
    }

    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.52.0"
    }
  }

  backend "s3" {
    bucket         = "sungchuni-terraform-backend-development"
    dynamodb_table = "sungchuni-terraform-backend"
    encrypt        = true
    key            = "coverages/infra/environments/development/terraform.tfstate"
    region         = "ap-northeast-2"
  }
}
