resource "aws_api_gateway_rest_api" "main" {
  api_key_source               = "HEADER"
  binary_media_types           = []
  disable_execute_api_endpoint = false
  minimum_compression_size     = -1
  name                         = "coverages"
  put_rest_api_mode            = "overwrite"

  endpoint_configuration {
    types = [
      "REGIONAL",
    ]
  }
}

resource "aws_api_gateway_resource" "schema" {
  parent_id   = aws_api_gateway_rest_api.main.root_resource_id
  path_part   = "schema"
  rest_api_id = aws_api_gateway_rest_api.main.id
}

resource "aws_api_gateway_resource" "project" {
  parent_id   = aws_api_gateway_rest_api.main.root_resource_id
  path_part   = "{project}"
  rest_api_id = aws_api_gateway_rest_api.main.id
}

resource "aws_api_gateway_resource" "branch" {
  parent_id   = aws_api_gateway_resource.project.id
  path_part   = "{branch}"
  rest_api_id = aws_api_gateway_rest_api.main.id
}

resource "aws_api_gateway_method" "head_schema" {
  api_key_required     = false
  authorization        = "NONE"
  authorization_scopes = []
  http_method          = "HEAD"
  request_models       = {}
  request_parameters = {
    "method.request.querystring.export_type" = false
  }
  resource_id = aws_api_gateway_resource.schema.id
  rest_api_id = aws_api_gateway_rest_api.main.id
}

resource "aws_api_gateway_method" "get_schema" {
  api_key_required     = false
  authorization        = "NONE"
  authorization_scopes = []
  http_method          = "GET"
  request_models       = {}
  request_parameters = {
    "method.request.querystring.export_type" = false
  }
  resource_id = aws_api_gateway_resource.schema.id
  rest_api_id = aws_api_gateway_rest_api.main.id
}

resource "aws_api_gateway_method" "head_item" {
  api_key_required     = false
  authorization        = "NONE"
  authorization_scopes = []
  http_method          = "HEAD"
  request_models       = {}
  request_parameters = {
    "method.request.path.branch"        = true
    "method.request.path.project"       = true
    "method.request.querystring.format" = false
  }
  resource_id = aws_api_gateway_resource.branch.id
  rest_api_id = aws_api_gateway_rest_api.main.id
}

resource "aws_api_gateway_method" "get_item" {
  api_key_required     = false
  authorization        = "NONE"
  authorization_scopes = []
  http_method          = "GET"
  request_models       = {}
  request_parameters = {
    "method.request.path.branch"        = true
    "method.request.path.project"       = true
    "method.request.querystring.format" = false
  }
  resource_id = aws_api_gateway_resource.branch.id
  rest_api_id = aws_api_gateway_rest_api.main.id
}

resource "aws_api_gateway_method" "post_item" {
  api_key_required     = false
  authorization        = "NONE"
  authorization_scopes = []
  http_method          = "POST"
  request_models = {
    "application/json" = aws_api_gateway_model.put_request.name
  }
  request_parameters = {
    "method.request.path.branch"  = true
    "method.request.path.project" = true
  }
  request_validator_id = aws_api_gateway_request_validator.body.id
  resource_id          = aws_api_gateway_resource.branch.id
  rest_api_id          = aws_api_gateway_rest_api.main.id
}

resource "aws_api_gateway_method" "put_item" {
  api_key_required     = false
  authorization        = "NONE"
  authorization_scopes = []
  http_method          = "PUT"
  request_models = {
    "application/json" = aws_api_gateway_model.put_request.name
  }
  request_parameters = {
    "method.request.path.branch"  = true
    "method.request.path.project" = true
  }
  request_validator_id = aws_api_gateway_request_validator.body.id
  resource_id          = aws_api_gateway_resource.branch.id
  rest_api_id          = aws_api_gateway_rest_api.main.id
}

resource "aws_api_gateway_method_settings" "all" {
  rest_api_id = aws_api_gateway_rest_api.main.id
  stage_name  = aws_api_gateway_stage.v1.stage_name
  method_path = "*/*"

  settings {
    metrics_enabled = true
    logging_level   = "INFO"
  }
}

resource "aws_api_gateway_integration" "head_schema" {
  cache_key_parameters    = []
  cache_namespace         = aws_api_gateway_resource.schema.id
  connection_type         = "INTERNET"
  content_handling        = "CONVERT_TO_TEXT"
  http_method             = aws_api_gateway_method.head_schema.http_method
  integration_http_method = "POST"
  passthrough_behavior    = "WHEN_NO_MATCH"
  request_parameters      = {}
  request_templates       = {}
  resource_id             = aws_api_gateway_resource.schema.id
  rest_api_id             = aws_api_gateway_rest_api.main.id
  timeout_milliseconds    = 29000
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.main.invoke_arn
}

resource "aws_api_gateway_integration" "get_schema" {
  cache_key_parameters    = []
  cache_namespace         = aws_api_gateway_resource.schema.id
  connection_type         = "INTERNET"
  content_handling        = "CONVERT_TO_TEXT"
  http_method             = aws_api_gateway_method.get_schema.http_method
  integration_http_method = "POST"
  passthrough_behavior    = "WHEN_NO_MATCH"
  request_parameters      = {}
  request_templates       = {}
  resource_id             = aws_api_gateway_resource.schema.id
  rest_api_id             = aws_api_gateway_rest_api.main.id
  timeout_milliseconds    = 29000
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.main.invoke_arn
}

resource "aws_api_gateway_integration" "head_item" {
  cache_key_parameters    = []
  cache_namespace         = aws_api_gateway_resource.branch.id
  connection_type         = "INTERNET"
  content_handling        = "CONVERT_TO_TEXT"
  http_method             = aws_api_gateway_method.head_item.http_method
  integration_http_method = "POST"
  passthrough_behavior    = "WHEN_NO_MATCH"
  request_parameters      = {}
  request_templates       = {}
  resource_id             = aws_api_gateway_resource.branch.id
  rest_api_id             = aws_api_gateway_rest_api.main.id
  timeout_milliseconds    = 29000
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.main.invoke_arn
}

resource "aws_api_gateway_integration" "get_item" {
  cache_key_parameters    = []
  cache_namespace         = aws_api_gateway_resource.branch.id
  connection_type         = "INTERNET"
  content_handling        = "CONVERT_TO_TEXT"
  http_method             = aws_api_gateway_method.get_item.http_method
  integration_http_method = "POST"
  passthrough_behavior    = "WHEN_NO_MATCH"
  request_parameters      = {}
  request_templates       = {}
  resource_id             = aws_api_gateway_resource.branch.id
  rest_api_id             = aws_api_gateway_rest_api.main.id
  timeout_milliseconds    = 29000
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.main.invoke_arn
}

resource "aws_api_gateway_integration" "post_item" {
  cache_key_parameters    = []
  cache_namespace         = aws_api_gateway_resource.branch.id
  connection_type         = "INTERNET"
  content_handling        = "CONVERT_TO_TEXT"
  http_method             = aws_api_gateway_method.post_item.http_method
  integration_http_method = "POST"
  passthrough_behavior    = "WHEN_NO_MATCH"
  request_parameters      = {}
  request_templates       = {}
  resource_id             = aws_api_gateway_resource.branch.id
  rest_api_id             = aws_api_gateway_rest_api.main.id
  timeout_milliseconds    = 29000
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.main.invoke_arn
}

resource "aws_api_gateway_integration" "put_item" {
  cache_key_parameters    = []
  cache_namespace         = aws_api_gateway_resource.branch.id
  connection_type         = "INTERNET"
  content_handling        = "CONVERT_TO_TEXT"
  http_method             = aws_api_gateway_method.put_item.http_method
  integration_http_method = "POST"
  passthrough_behavior    = "WHEN_NO_MATCH"
  request_parameters      = {}
  request_templates       = {}
  resource_id             = aws_api_gateway_resource.branch.id
  rest_api_id             = aws_api_gateway_rest_api.main.id
  timeout_milliseconds    = 29000
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.main.invoke_arn
}

resource "aws_api_gateway_method_response" "head_schema_200" {
  http_method = aws_api_gateway_method.head_schema.http_method
  resource_id = aws_api_gateway_resource.schema.id
  rest_api_id = aws_api_gateway_rest_api.main.id
  response_models = {
    "application/json" = "Empty"
  }
  response_parameters = {}
  status_code         = "200"
}

resource "aws_api_gateway_method_response" "get_schema_200" {
  http_method = aws_api_gateway_method.get_schema.http_method
  resource_id = aws_api_gateway_resource.schema.id
  rest_api_id = aws_api_gateway_rest_api.main.id
  response_models = {
    "application/json" = "Empty"
  }
  response_parameters = {}
  status_code         = "200"
}

resource "aws_api_gateway_method_response" "head_item_200" {
  http_method = aws_api_gateway_method.head_item.http_method
  resource_id = aws_api_gateway_resource.branch.id
  rest_api_id = aws_api_gateway_rest_api.main.id
  response_models = {
    "application/json" = aws_api_gateway_model.get_response.name,
    "image/svg+xml"    = "Empty"
  }
  response_parameters = {}
  status_code         = "200"
}

resource "aws_api_gateway_method_response" "get_item_200" {
  http_method = aws_api_gateway_method.get_item.http_method
  resource_id = aws_api_gateway_resource.branch.id
  rest_api_id = aws_api_gateway_rest_api.main.id
  response_models = {
    "application/json" = aws_api_gateway_model.get_response.name,
    "image/svg+xml"    = "Empty"
  }
  response_parameters = {}
  status_code         = "200"
}

resource "aws_api_gateway_method_response" "post_item_200" {
  http_method         = aws_api_gateway_method.post_item.http_method
  resource_id         = aws_api_gateway_resource.branch.id
  rest_api_id         = aws_api_gateway_rest_api.main.id
  response_models     = {}
  response_parameters = {}
  status_code         = "200"
}

resource "aws_api_gateway_method_response" "put_item_200" {
  http_method         = aws_api_gateway_method.put_item.http_method
  resource_id         = aws_api_gateway_resource.branch.id
  rest_api_id         = aws_api_gateway_rest_api.main.id
  response_models     = {}
  response_parameters = {}
  status_code         = "200"
}

resource "aws_api_gateway_request_validator" "body" {
  name                        = "본문 검사"
  rest_api_id                 = aws_api_gateway_rest_api.main.id
  validate_request_body       = true
  validate_request_parameters = false
}

resource "aws_api_gateway_model" "get_response" {
  rest_api_id  = aws_api_gateway_rest_api.main.id
  content_type = "application/json"
  name         = "CoveragesGetResponse"
  schema       = file("./get-response.schema.json")
}

resource "aws_api_gateway_model" "put_request" {
  rest_api_id  = aws_api_gateway_rest_api.main.id
  content_type = "application/json"
  name         = "CoveragesPutRequest"
  schema       = file("./put-request.schema.json")
}

resource "aws_api_gateway_deployment" "main" {
  rest_api_id = aws_api_gateway_rest_api.main.id

  triggers = {
    redeployment = sha1(jsonencode([
      aws_api_gateway_method.head_schema,
      aws_api_gateway_method.get_schema,
      aws_api_gateway_method.get_item,
      aws_api_gateway_method.post_item,
      aws_api_gateway_method.put_item,
      aws_api_gateway_integration.head_schema,
      aws_api_gateway_integration.get_schema,
      aws_api_gateway_integration.head_item,
      aws_api_gateway_integration.get_item,
      aws_api_gateway_integration.post_item,
      aws_api_gateway_integration.put_item,
      aws_api_gateway_method_response.head_schema_200,
      aws_api_gateway_method_response.get_schema_200,
      aws_api_gateway_method_response.head_item_200,
      aws_api_gateway_method_response.get_item_200,
      aws_api_gateway_method_response.post_item_200,
      aws_api_gateway_method_response.put_item_200,
    ]))
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_api_gateway_stage" "v1" {
  cache_cluster_enabled = false
  deployment_id         = aws_api_gateway_deployment.main.id
  rest_api_id           = aws_api_gateway_rest_api.main.id
  stage_name            = "v1"
  variables             = {}
  xray_tracing_enabled  = false
}

resource "aws_api_gateway_base_path_mapping" "v1" {
  domain_name = aws_api_gateway_domain_name.coverages_sungchuni_site.id
  api_id      = aws_api_gateway_rest_api.main.id
  stage_name  = aws_api_gateway_stage.v1.stage_name
  base_path   = "v1"
}

resource "aws_api_gateway_domain_name" "coverages_sungchuni_site" {
  domain_name              = "coverages.sungchuni.site"
  regional_certificate_arn = data.aws_acm_certificate.coverages_sungchuni_site.arn

  endpoint_configuration {
    types = ["REGIONAL"]
  }
}

data "aws_acm_certificate" "coverages_sungchuni_site" {
  domain    = "coverages.sungchuni.site"
  key_types = ["EC_prime256v1"]
  statuses  = ["ISSUED"]
  types     = ["IMPORTED"]
}

resource "aws_api_gateway_account" "main" {
  cloudwatch_role_arn = aws_iam_role.cloudwatch.arn
}
