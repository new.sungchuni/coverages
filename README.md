# coverages

테스트 커버리지를 저장하고 조회, 커버리지 배지를 제공하는 서비스입니다.

[Shields.io](https://shields.io)의 [badge-maker](https://www.npmjs.com/package/badge-maker)를 사용하여 (수 제한이 없이) 1개 이상의 커버리지 배지 이미지를 생성하고 각 svg 이미지를 연결하여 한 번에 제공합니다.

## 요구사항

- nodejs `^18.14.2`
- npm `^9.5.0`
- terraform `>= 1.3.9`

## 사용

- /schema 리소스에 대한 GET 요청으로 이 서비스의 OpenAPI 3 스키마를 조회할 수 있습니다.
[swagger editor](https://editor-next.swagger.io/)의 import url 기능을 이용해보세요.
```sh
$ curl "https://coverages.sungchuni.site/v1/schema"
```

- /{project}/{branch} 리소스에 대한 POST 혹은 PUT 요청으로 커버리지를 입력할 수 있습니다.
요청 페이로드는 위 스키마를 참조하십시오.
```sh
$ curl \
  -X POST \
  -H "content-type: application/json" \
  -d '
  [
    {
      "packageName": "",
      "summary": {
        "total": {
          "statements": {
            "pct": 99.99
          }
        }
      }
    },
    {
      "packageName": "@sungchuni/app-nothing",
      "summary": {
        "total": {
          "statements": {
            "pct": 97
          }
        }
      }
    }
  ]
  ' \
  "https://coverages.sungchuni.site/v1/coverages/main"
```

- /{project}/{branch} 리소스에 대한 GET 요청으로 커버리지를 조회할 수 있습니다.
```sh
$ curl "https://coverages.sungchuni.site/v1/coverages/main"
```
format query parameter에 `svg` 값을 전송하거나, accept header를 `image/svg+xml` 로 지정하여 커버리지 배지 이미지를 수신할 수 있습니다.
```sh
$ curl -H "accept: image/svg+xml" "https://coverages.sungchuni.site/v1/coverages/main"
```

- 각 어플리케이션 빌드 파이프라인에서 테스트 수행 후 테스트 커버리지를 생성하고 이 서비스에 커버리지를 전송하도록 구성합니다.
  - istanbul reporter를 사용할 수 있는 환경이라면 json-summary 형식으로 커버리지 파일을 생성, 요청 페이로드에 맞게 변환한 후 POST 혹은 PUT 요청을 전송하면 됩니다.
  - istanbul reporter를 사용할 수 없는 환경이라면 테스트 수행 후 커버리지를 직접 계산하여 요청 페이로드에 맞게 변환한 후 POST 혹은 PUT 요청을 전송하면 됩니다.

- 어플리케이션의 README.md 파일에 아래와 같이 배지 이미지를 추가합니다.
```md
[![coverages](https://coverages.sungchuni.site/v1/coverages/main?format=svg)](https://coverages.sungchuni.site/v1/coverages/main)
```

## 구성

- aws apigateway rest api
  - `HEAD /schema`
  - `GET /schema`
  - `HEAD /{project}/{branch}`
  - `POST /{project}/{branch}`
  - `PUT /{project}/{branch}`
- aws lambda nodejs function
  - `coverages`
- aws dynamodb table
  - `coverages`, partition key: `project`, sort key: `branch`

## 빌드 및 배포

```sh
$ npm run deploy
```

## 테스트

```sh
$ npm run test
```

## 작성자

[sungchuni](new.sungchuni@gmail.com)
